var socket = io("localhost:3000");
var user = {favorites: [], queue: [], friends: []};


SC.initialize({
    client_id: '9ebe4a9214456dfe7e7c9508a6520691',
    redirect_uri: 'http://localhost:3000/callback.html'
});

$(document).ready(function() {
    socket.on('event', function(data) {
       console.log(data);
    });


    /**
     * Handle the user login
     */
   $('.btn-auth').click(function() {
       SC.connect().then(function(){
           SC.get('/me').then(function(me) {
               user.name = me.username;
               user.id = me.id
               console.log(me);
               socket.emit("online", {user: user.name});
               $('.btn-auth').slideUp();
               $('.btn-show-after-auth').slideToggle();
           });
       });
   });
    /**
     * Load favorties
     */
    $('.btn-fav').click(function() {
       SC.get('/users/' + user.id + '/favorites	').then(function(data) {
           data.forEach(function(track) {
               $('.favs').append(
                   "<li class='list-group-item track' id=" + track.id + ">" +
                   "<span class='badge'>" + track.user.username + "</span>" +
                   "<div>" + track.title + "</div>" +
                   "</li>"
               );
               user.favorites[track.id] = track;
           });
       }
       );
    });
    /**
     * Add friend
     */
    $('.btn-add-friend').click(function() {
        socket.emit("Add", {friend: $('.form-user').val()});
        $('.form-user').val("");
    });
});
